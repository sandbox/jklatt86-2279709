-- SUMMARY --

A module to temporarily disable your website front-end and display a message to
your visitors while still allowing back-end access. This module can be enabled
or disabled with one click and also supports using your own custom HTML/CSS
output for the splash page so you can make it as simple or as custom as you
need it to be.

This module is especially useful if you need to temporarily make your site
unavailable to your visitors. For example, you're working on a new feature or
website and you want to show a temporary "coming soon" page to your visitors
while you're working on it; or you're fixing major bugs that would disrupt the
user experience and need to make the front-end temporarily unavailable; or any
other reason that you would need to make your website temporarily unavailable.

This module can be configured to allow admin users (you) to still be able to
view the front-end while the splash page is enabled. If other users need to be
able to review the site, you can enable authenticated users and/or specific IP
addresses to be able to bypass the splash page as well.

Common uses for this module:
* Displaying a splash page while you developer your blog or website
* Temporarily making your site unavailable to visitors while you fix bugs, etc
* Disabling a site if a client refuses to pay (we've all been there)

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual.
* Activate the module through the Modules screen in Drupal.
* Configure the module.
* Profit.

-- TROUBLESHOOTING --

* If you can't figure out how to log in to your website while the splash page
  is enabled, navigate to one of the following:

  - http://www.yourwebsite.com/user/login
  - http://www.yourwebsite.com/?q=user/login

-- FAQ --

Q: How do I change the default splash page message?

A: Navigate to the configuration page for the module and set the textfield
   labeled Custom Message to your custom message.

Q: How do I specify my own HTML for the splash page?
A: Navigate to the configuration page for the module and paste your HTML and/or
   CSS into the field labeled Custom HTML Output.

Q: Can my splash page include HTML, CSS, and JavaScript?
A: Yes it can! You can use any HTML, CSS, and/or JavaScript that you need.

Q: How do I disable the splash page?
A: Navigate to the configuration page for the module and set the dropdown
   labeled Enable Splash Page to No.

Q: How do I allow administrators to bypass the splash page?
A: Navigate to the configuration page for the module and set the dropdown
   labeled Admin Access to Yes.

Q: How do I allow authenticated users to bypass the splash page?
A: Navigate to the configuration page for the module and set the dropdown
   labeled Authenticated User Access to Yes.

Q: How do I allow users from specific IP addresses to bypass the splash page?
A: Navigate to the configuration page for the module and enter each IP address
  into the textarea labeled Allowed IPs (each one on it's own line).

-- CONTACT --

Current maintainers:
* Jimmy K. (jklatt86) - http://drupal.org/user/2614334

This project has been sponsored by:
* Orbit Media Studios
  Visit http://www.orbitmedia.com for more information.
